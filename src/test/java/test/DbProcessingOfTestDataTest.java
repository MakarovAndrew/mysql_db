package test;

import lombok.extern.slf4j.Slf4j;
import models.TestModel;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.TestRunSimulator;
import utils.RandomUtils;
import java.util.ArrayList;
import java.util.List;
import static utils.ConfigManager.*;

@Slf4j
@Test (testName = "MA-72 Verify that the test data has been processed correctly")
public class DbProcessingOfTestDataTest extends BaseTest {
    private List<TestModel> testResults = new ArrayList<>();

    @BeforeTest
    public void copyTestsWithRepeatingDigit() {
        log.info("Making TC-2 preconditions...");
        final int MAX_VALUE = 9;
        final int SELECTED_ROWS_LIMIT = 10;
        final int digit = RandomUtils.randomNumberMaxIncluded(0, MAX_VALUE);
        final int repeatedDigit = Integer.parseInt(String.format("%1$s%1$s", digit));
        testResults = testDao.getLimitedCountOfTestsWhereIdContainsValue(repeatedDigit, SELECTED_ROWS_LIMIT);

        for (TestModel testModel : testResults) {
            testModel.setProject_id(testProject.getId());
            testModel.setAuthor_id(testAuthor.getId());
            testModel.setId(testDao.insert(testModel));
        }
    }

    public void checkThatSimulatedDataUpdatedCorrectly() {
        log.info("Checking that the simulated test results are updated in corresponding database...");
        List<TestModel> testFromDb = new ArrayList<>();

        for (TestModel testModel : testResults) {
            testDao.update(TestRunSimulator.simulateTestPass(testModel));
            testFromDb.add(testDao.get((int) testModel.getId()));
        }
        Assert.assertTrue(testResults.size() == testFromDb.size() && testFromDb.containsAll(testResults),
                "Simulated tests have not updated correctly!");
    }
}