package test;

import lombok.extern.slf4j.Slf4j;
import models.TestModel;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import utils.Browsers;
import utils.RandomUtils;
import static utils.ConfigManager.*;
import static utils.TestDateFormatter.*;

@Slf4j
@Test (testName = "MA-72 Verify that new entry has been added")
public class DbAddingNewEntryTest extends BaseTest {
    private TestModel anyTestResult;

    public void anyTest() {
        log.info("Starting any test...");
        Assert.assertTrue(RandomUtils.randomBoolean(), "The Random boolean value is not true!");
    }

    @AfterMethod
    public void getAnyTestResult(ITestResult result){
        anyTestResult = TestModel.builder()
                .name(result.getTestName())
                .status_id(result.getStatus())
                .method_name(result.getMethod().getQualifiedName())
                .project_id(testProject.getId())
                .session_id(testSession.getId())
                .start_time(longDateToString(result.getStartMillis()))
                .end_time(longDateToString(result.getEndMillis()))
                .env(System.getenv("COMPUTERNAME"))
                .browser(Browsers.getRandomBrowser())
                .author_id(testAuthor.getId())
                .build();
    }

    @AfterClass
    public void anyTestPostConditions() {
        log.info("Checking that the test result added in the database as a new record in the corresponding table...");
        anyTestResult.setId(testDao.insert(anyTestResult));
        TestModel anyTestResultFromDb = testDao.get(anyTestResult.getId());
        Assert.assertEquals(anyTestResultFromDb, anyTestResult, "Test results are not equal!" +
                "\nExpected: " + anyTestResult.toString() +
                "\nActual  : " + anyTestResultFromDb.toString());
        }
}