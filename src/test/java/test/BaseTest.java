package test;

import dao.AuthorDao;
import dao.ProjectDao;
import dao.SessionDao;
import dao.TestDao;
import org.testng.annotations.*;
import dbms.MySqlConnector;
import static utils.ConfigManager.*;

public abstract class BaseTest {
    private final ProjectDao projectDao = new ProjectDao();
    private final SessionDao sessionDao = new SessionDao();
    private final AuthorDao authorDao = new AuthorDao();
    protected final TestDao testDao = new TestDao();

    @BeforeSuite
    protected void beforeSuite() {
        testProject.setId(projectDao.insert(testProject));
        testSession.setId(sessionDao.insert(testSession));
        testAuthor.setId(authorDao.insert(testAuthor));
    }

    @AfterSuite
    protected void removeTestPreconditions() {
        projectDao.delete(testProject);
        sessionDao.delete(testSession);
        authorDao.delete(testAuthor);
    }

    @AfterTest
    protected void closeDbConnection() {
        MySqlConnector.closeConnection();
    }
}