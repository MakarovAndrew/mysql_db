package models;

import lombok.*;

@Getter @ToString
public class ProjectModel {
    private @NonNull @Setter long id;
    private @NonNull String name;
}