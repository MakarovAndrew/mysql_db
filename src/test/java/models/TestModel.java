package models;

import lombok.*;

@Builder @ToString @Getter @EqualsAndHashCode
public class TestModel {
    private @NonNull @Setter long id;
    private @NonNull String name;
    private @Setter int status_id;
    private @NonNull String method_name;
    private @NonNull @Setter long project_id;
    private @NonNull long session_id;
    private String start_time;
    private String end_time;
    private @NonNull String env;
    private @Setter String browser;
    private @Setter long author_id;
}