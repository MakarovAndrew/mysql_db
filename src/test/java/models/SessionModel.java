package models;

import lombok.*;

@Getter @ToString
public class SessionModel {
    private @NonNull @Setter long id;
    private @NonNull String session_key;
    private @NonNull String created_time;
    private @NonNull long build_number;
}