package models;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter @ToString
public class AuthorModel {
    private @NonNull @Setter long id;
    private @NonNull String name;
    private @NonNull String login;
    private @NonNull String email;
}