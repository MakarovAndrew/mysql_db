package dao;

import lombok.extern.slf4j.Slf4j;
import models.AuthorModel;
import utils.DbUtils;
import utils.SqlFileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import static utils.ConfigManager.PATH_TO_SQL_DIRECTORY;

@Slf4j
public class AuthorDao {

    public long insert(AuthorModel authorModel) {
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/insertAuthor.sql");
        final String sql = String.format(
                SqlFileReader.readQueryFromFile(PATH_TO_SQL),
                    authorModel.getName(),
                    authorModel.getLogin(),
                    authorModel.getEmail()
        );
        return DbUtils.executeUpdateAndGetLongGeneratedKey(sql);
    }

    public void delete(AuthorModel authorModel) {
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/deleteAuthorById.sql");
        final String sql = String.format(SqlFileReader.readQueryFromFile(PATH_TO_SQL), authorModel.getId());
        DbUtils.executeUpdateAndGetLongGeneratedKey(sql);
        log.info("Author \"" + authorModel.getName() + "\" successfully deleted from DB");
    }
}