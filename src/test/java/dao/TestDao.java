package dao;

import lombok.extern.slf4j.Slf4j;
import models.TestModel;
import utils.DbUtils;
import utils.SqlFileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import static utils.TestDateFormatter.*;
import static utils.ConfigManager.PATH_TO_SQL_DIRECTORY;

@Slf4j
public class TestDao {

    public TestModel get(long id) {
        TestModel testModel = null;
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/selectTestById.sql");
        final String sql = String.format(SqlFileReader.readQueryFromFile(PATH_TO_SQL), id);
        ResultSet rs = DbUtils.getResultSet(sql);
        try {
            if (rs.next()) {
                testModel = TestModel.builder()
                        .id(rs.getLong("id"))
                        .name(rs.getString("name"))
                        .status_id(rs.getInt("status_id"))
                        .method_name(rs.getString("method_name"))
                        .project_id(rs.getLong("project_id"))
                        .session_id(rs.getLong("session_id"))
                        .start_time(rs.getString("start_time"))
                        .end_time(rs.getString("end_time"))
                        .env(rs.getString("env"))
                        .browser(rs.getString("browser"))
                        .author_id(rs.getLong("author_id"))
                        .build();
                log.info("Test \"" + testModel.getName() + "\" successfully read from DB with id: " + testModel.getId());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return testModel;
    }
    public List<TestModel> getLimitedCountOfTestsWhereIdContainsValue(int repeatedValue, int limit) {
        List<TestModel> list = new ArrayList<>();
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/selectLimitedCountOfTestsWhereIdContainsValue.sql");
        final String sql = String.format(SqlFileReader.readQueryFromFile(PATH_TO_SQL), repeatedValue, limit);
        ResultSet rs = DbUtils.getResultSet(sql);
        try {
            while (rs.next()) {
                TestModel testModel = TestModel.builder()
                        .id(rs.getLong("id"))
                        .name(rs.getString("name"))
                        .status_id(rs.getInt("status_id"))
                        .method_name(rs.getString("method_name"))
                        .project_id(rs.getLong("project_id"))
                        .session_id(rs.getLong("session_id"))
                        .start_time(rs.getString("start_time"))
                        .end_time(rs.getString("end_time"))
                        .env(rs.getString("env"))
                        .browser(rs.getString("browser"))
                        .author_id(rs.getLong("author_id"))
                        .build();
                list.add(testModel);
            }
            log.info("Tests with repeated digits in id: \"" + repeatedValue + "\" successfully read from DB");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public long insert(TestModel testModel) {
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/insertTest.sql");
        final Timestamp startSqlDate = new Timestamp(stringDateToLong(testModel.getStart_time()));
        final Timestamp endSqlDate = new Timestamp(stringDateToLong(testModel.getEnd_time()));
        final String sql = String.format(
                SqlFileReader.readQueryFromFile(PATH_TO_SQL),
                    testModel.getName(),
                    testModel.getStatus_id(),
                    testModel.getMethod_name(),
                    testModel.getProject_id(),
                    testModel.getSession_id(),
                    startSqlDate,
                    endSqlDate,
                    testModel.getEnv(),
                    testModel.getBrowser(),
                    testModel.getAuthor_id()
        );
        return DbUtils.executeUpdateAndGetLongGeneratedKey(sql);
    }

    public void update(TestModel testModel) {
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/updateTestById.sql");
        final Timestamp startSqlDate = new Timestamp(stringDateToLong(testModel.getStart_time()));
        final Timestamp endSqlDate = new Timestamp(stringDateToLong(testModel.getEnd_time()));
        final String sql = String.format(
                SqlFileReader.readQueryFromFile(PATH_TO_SQL),
                    testModel.getName(),
                    testModel.getStatus_id(),
                    testModel.getMethod_name(),
                    testModel.getProject_id(),
                    testModel.getSession_id(),
                    startSqlDate,
                    endSqlDate,
                    testModel.getEnv(),
                    testModel.getBrowser(),
                    testModel.getAuthor_id(),
                    testModel.getId()
        );
        DbUtils.executeUpdateAndGetLongGeneratedKey(sql);
        log.info("Test \"" + testModel.getName() + "\" successfully updated");
    }
}