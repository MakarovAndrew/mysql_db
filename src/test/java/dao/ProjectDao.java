package dao;

import lombok.extern.slf4j.Slf4j;
import models.ProjectModel;
import utils.DbUtils;
import utils.SqlFileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import static utils.ConfigManager.PATH_TO_SQL_DIRECTORY;

@Slf4j
public class ProjectDao {

    public long insert(ProjectModel projectModel) {
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/insertProject.sql");
        final String sql = String.format(SqlFileReader.readQueryFromFile(PATH_TO_SQL), projectModel.getName());
        return DbUtils.executeUpdateAndGetLongGeneratedKey(sql);
    }

    public void delete(ProjectModel projectModel) {
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/deleteProjectById.sql");
        final String sql = String.format(SqlFileReader.readQueryFromFile(PATH_TO_SQL), projectModel.getId());
        DbUtils.executeUpdateAndGetLongGeneratedKey(sql);
        log.info("Project \"" + projectModel.getName() + "\" successfully deleted from DB");
    }
}