package dao;

import lombok.extern.slf4j.Slf4j;
import models.SessionModel;
import utils.DbUtils;
import utils.SqlFileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import static utils.TestDateFormatter.*;
import static utils.ConfigManager.PATH_TO_SQL_DIRECTORY;

@Slf4j
public class SessionDao {

    public long insert(SessionModel sessionModel) {
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/insertSession.sql");
        final Timestamp sqlDate = new Timestamp(stringDateToLong(sessionModel.getCreated_time()));
        final String sql = String.format(
                SqlFileReader.readQueryFromFile(PATH_TO_SQL),
                sessionModel.getSession_key(),
                sqlDate,
                sessionModel.getBuild_number()
        );
        return DbUtils.executeUpdateAndGetLongGeneratedKey(sql);
    }

    public void delete(SessionModel sessionModel) {
        final Path PATH_TO_SQL = Paths.get(PATH_TO_SQL_DIRECTORY + "/deleteSessionById.sql");
        final String sql = String.format(SqlFileReader.readQueryFromFile(PATH_TO_SQL), sessionModel.getId());
        DbUtils.executeUpdateAndGetLongGeneratedKey(sql);
        log.info("Session \"" + sessionModel.getSession_key() + "\" successfully deleted from DB");
    }
}