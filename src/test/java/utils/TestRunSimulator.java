package utils;

import models.TestModel;

public class TestRunSimulator {
    private final static int MAX_VALUE_OF_STATUS_CODE = 3;
    private final static int MIN_VALUE_OF_STATUS_CODE = 1;

    public static TestModel simulateTestPass(TestModel model) {
        model.setStatus_id(RandomUtils.randomNumberMaxIncluded(MIN_VALUE_OF_STATUS_CODE, MAX_VALUE_OF_STATUS_CODE));
        model.setBrowser(Browsers.getRandomBrowser());
        return model;
    }
}