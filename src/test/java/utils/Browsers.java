package utils;

import java.util.Random;

public enum Browsers {
    CHROME, OPERA, FIREFOX, SAFARI;

    public static String getRandomBrowser() {
        Browsers[] browsers = values();
        return browsers[new Random().nextInt(browsers.length)].name().toLowerCase();
    }
}