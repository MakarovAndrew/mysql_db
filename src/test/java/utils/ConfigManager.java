package utils;

import models.AuthorModel;
import models.ProjectModel;
import models.SessionModel;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigManager {
    private final static Path PATH_TO_TEST_AUTHOR = Paths.get("src/resources/testAuthor.json");
    private final static Path PATH_TO_TEST_PROJECT = Paths.get("src/resources/testProject.json");
    private final static Path PATH_TO_TEST_SESSION = Paths.get("src/resources/testSession.json");
    public final static Path PATH_TO_SQL_DIRECTORY = Paths.get("src/resources/sqlQueries");
    public final static AuthorModel testAuthor = JsonUtils.readObjectFromJsonFile(PATH_TO_TEST_AUTHOR, AuthorModel.class);
    public final static ProjectModel testProject = JsonUtils.readObjectFromJsonFile(PATH_TO_TEST_PROJECT, ProjectModel.class);
    public final static SessionModel testSession = JsonUtils.readObjectFromJsonFile(PATH_TO_TEST_SESSION, SessionModel.class);
}