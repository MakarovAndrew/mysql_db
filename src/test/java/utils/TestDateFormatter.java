package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestDateFormatter {
    private final static String FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String longDateToString(long date) {
        return new SimpleDateFormat(FORMAT).format(date);
    }

    public static long stringDateToLong(String date) {
        try {
            return new SimpleDateFormat(FORMAT).parse(date).getTime();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}