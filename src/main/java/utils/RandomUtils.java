package utils;

import static org.apache.commons.lang3.RandomUtils.*;

public class RandomUtils {

    public static int randomNumberMaxIncluded(int min, int max) {
        return nextInt(min, max+1);
    }

    public static boolean randomBoolean() {
        return nextBoolean();
    }
}