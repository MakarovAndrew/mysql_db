package utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;
import java.nio.file.Path;

@Slf4j
public class JsonUtils {
    private static final ObjectMapper mapper = new ObjectMapper().enable(DeserializationFeature.FAIL_ON_TRAILING_TOKENS);

    public static <T> T readObjectFromJsonFile(Path pathToJson, Class<T> clazz) {
        try {
            log.info("Reading Object from file :: \"" + pathToJson + "\" ...");
            return mapper.readValue(pathToJson.toFile(), clazz);
        } catch (IOException e) {
            throw new RuntimeException("Wrong file format or file not found by path: " + pathToJson);
        }
    }
}