package utils;

import org.apache.commons.io.FileUtils;
import java.io.*;
import java.nio.file.Path;

public class SqlFileReader {
    public static String readQueryFromFile(Path pathToSqlQuery) {
        final File file = pathToSqlQuery.toFile();
        String query = null;
        try {
            query = FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return query;
    }
}