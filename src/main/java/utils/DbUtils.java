package utils;

import dbms.MySqlConnector;
import java.sql.*;

public class DbUtils {

    public static long executeUpdateAndGetLongGeneratedKey(String sqlQuery) {
        ResultSet resultSet;
        try {
            PreparedStatement preparedStatement = MySqlConnector.getInstance().getConnection().prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    return resultSet.getLong("GENERATED_KEY");
                } else {
                    return 0;
                }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static ResultSet getResultSet(String sqlQuery) {
        try {
            return MySqlConnector.getInstance().getConnection().prepareStatement(sqlQuery).executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}