package utils;

import models.DbmsConfig;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DbmsConfigManager {
    private final static Path PATH_TO_DBMS_CONFIG = Paths.get("src/resources/dbConnectionConfig.json");
    public final static DbmsConfig dbmsConfig = JsonUtils.readObjectFromJsonFile(PATH_TO_DBMS_CONFIG, DbmsConfig.class);
}