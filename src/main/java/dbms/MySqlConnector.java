package dbms;

import lombok.extern.slf4j.Slf4j;
import utils.DbmsConfigManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Slf4j
public class MySqlConnector {
    private static MySqlConnector instance;
    private Connection connection;
    private static final String dbUrl = DbmsConfigManager.dbmsConfig.getDbUrl();
    private static final String user = DbmsConfigManager.dbmsConfig.getDbmsSuperUserName();
    private static final String password = DbmsConfigManager.dbmsConfig.getDbmsSuperUserPassword();

    private MySqlConnector() {
        try {
            this.connection = DriverManager.getConnection(dbUrl, user, password);
            log.info("Connection :: \"" + dbUrl + "\" established successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public static MySqlConnector getInstance() {
        if (instance == null) {
            instance = new MySqlConnector();
        } else {
            try {
                if (instance.getConnection().isClosed()) {
                    instance = new MySqlConnector();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public static void closeConnection() {
        try {
            getInstance().getConnection().close();
            log.info("DB connection has successfully closed");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}