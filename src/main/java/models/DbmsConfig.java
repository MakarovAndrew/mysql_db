package models;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor @Getter
public class DbmsConfig {
    private String driverName;
    private String dbmsName;
    private String dbmsAddress;
    private String dbmsPort;
    @Getter private String dbmsSuperUserName;
    @Getter private String dbmsSuperUserPassword;
    private String dbName;

    public String getDbUrl() {
        return this.driverName + ":" + this.dbmsName + "://" + this.dbmsAddress + ":" + this.dbmsPort + "/" + this.dbName;
    }
}